﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSVParser
{
    class Program
    {
        static void Main(string[] args)
        {

            string filePath = CSVParser.Properties.Settings.Default.FilePath;

            var datas = File.ReadAllLines(filePath);
            var infolist = from datainfo in datas
                           let data = datainfo.Split(';')
                           select new
                           {
                               RowNo = data[0],
                               FirstName = data[1],
                               MiddleName = data[2],
                               LastName = data[3],
                               CompanyName = data[4],
                               GroupName = data[5],
                               GroupUserName = data[6],
                               GroupCode = data[7],
                               UserId = data[8],
                               Pesel = data[9]
                           };

            foreach(var info in infolist)
            {
                Console.WriteLine(info.RowNo + "|" + info.FirstName + "|" + info.MiddleName +"|" 
                    + info.LastName + "|" + info.CompanyName +"|"+ info.GroupName +"|" + info.GroupUserName + "|" + info.GroupCode + "|" +
                    info.UserId + "|" + info.Pesel);
            }

          

            
        }
    }
}
